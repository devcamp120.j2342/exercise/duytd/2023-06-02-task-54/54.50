package com.devcamp.s5450.country_region;

import java.util.ArrayList;

import com.devcamp.s5450.country_region.models.Country;
import com.devcamp.s5450.country_region.models.Region;

public class App {

    public static void main(String[] args) {
        ArrayList<Country> countries = new ArrayList<>();

        // Tạo các region cho Việt Nam
        ArrayList<Region> vietnamRegions = new ArrayList<>();
        vietnamRegions.add(new Region("HN", "Hanoi"));
        vietnamRegions.add(new Region("HCM", "Ho Chi Minh City"));

        // Tạo đối tượng Country cho Việt Nam
        Country vietnam = new Country("VN", "Vietnam", vietnamRegions);
    
        // Thêm Việt Nam vào danh sách các country
        countries.add(vietnam);

        // Duyệt danh sách các country và in ra danh sách các regions của Việt Nam
        for (Country country : countries) {
            if (country.getCountryName().equals("Vietnam")) {
                System.out.println("Country: " + country.getCountryName());
                System.out.println("Regions: ");
                for (Region region : country.getRegions()) {
                    System.out.println("- " + region.getRegionName());
                }
            }
        }
    }

}
